jQuery(document).ready(function($) {

	// get parameters
	var first = true;
	var url = getUrlParameter('url');
	var autoplay = getUrlParameter('autoplay');
	var radio = getUrlParameter('radio');
	var podcast = getUrlParameter('podcast');
	var podcastlink = getUrlParameter('podcastlink');
	var nologo = getUrlParameter('nologo');
	var logo = getUrlParameter('logo');

	if (nologo) {
		$(".xrcbplayer .logo").css("color", "transparent");
	}

	if (logo && typeof logo === "string") {

		$(".xrcbplayer .logo").css("pointer-events", "none");

		if (logo === "no") {
			// hide logo
			$(".xrcbplayer .logo").css("color", "transparent");
		}
		else if (logo.indexOf("http") !== -1) {
			// url logo image
			$(".xrcbplayer .logo").css("color", "transparent");
			$(".xrcbplayer .logo").css("background-image", "url('"+logo+"')");
		}
		else {
			// show logo text
			$(".xrcbplayer .logo").text(logo);
		}
	}

	$('#xrcbplayer').mediaelementplayer({
		alwaysShowControls: true,
		features: ['playpause','volume','current','progress','duration'],
		audioVolume: 'horizontal',
		audioWidth: 400,
		audioHeight: 60,
		hideVolumeOnTouchDevices: false,
		success: function(mediaElement, originalNode, instance) {
			console.log(mediaElement.id, "loaded");
			player = $("#"+mediaElement.id)[0];

			if (url) {
				player.setSrc(url);
			}
			if (autoplay == "true") {
				player.play();
			}
			if (radio) {
				$(".xrcbplayer .radio").text(radio);
			}
			if (podcast) {
				$(".xrcbplayer .podcast").text(podcast);
			}
			if (radio && player) {
				$(".xrcbplayer .sep").show();
			}
			if (podcastlink) {
				$(".xrcbplayer .podcast-link").text(podcastlink);
			}

            $(".xrcbplayer .logo").click(function() {
            	var link = document.createElement('a');
			    link.href = "https://xrcb.cat/";
			    link.target = "_blank";
			    document.body.appendChild(link);
			    link.click();
            });

            if (logo === undefined) {
	            $(".xrcbplayer h1").click(function() {
	            	var link = document.createElement('a');
	            	if (podcastlink) {
					    link.href = podcastlink;
	            	}
	            	else {
					    link.href = "https://xrcb.cat/";
	            	}
				    link.target = "_blank";
				    document.body.appendChild(link);
				    link.click();
	            });
	            $(".xrcbplayer h1").css("cursor", "pointer");
	        }
	        else {
	            $(".xrcbplayer h1").css("pointer-events", "none");
	        }	
		}
	});

	// track mp3 files for embedded players
	$(".xrcbplayer .mejs__play button").click(function() {
		if (first &&
			(jQuery(this).attr("title") == "Reprodueix" ||
			jQuery(this).attr("title") == "Reproducir" ||
			jQuery(this).attr("title") == "Play")) {

			// track podcast plays
			_paq.push(['trackLink', url, 'download', {dimension1: podcast } ]);
			//console.log('track', url, podcast);
			first = false;
		}
	});

});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
