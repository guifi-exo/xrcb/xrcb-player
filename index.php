<!DOCTYPE html>
<html lang="en">

<?php require_once('/var/www/html/wordpress/wp-load.php'); ?>

<head>
	<meta charset="utf-8">

	<title>XRCB audio player</title>

	<link rel="stylesheet" type="text/css" href="MyFontsWebfontsKit.css">
	<link rel="stylesheet" media="screen" href="player.css">

	<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/mediaelement@4.2.16/build/mediaelement-and-player.min.js"></script>
	<script src="player.js"></script>
</head>

<body>
	<!-- Matomo -->
	<script type="text/javascript">
	var _paq = window._paq || [];
	/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
	var u="https://matomo.xrcb.cat/";
	_paq.push(['setTrackerUrl', u+'piwik.php']);
	_paq.push(['setSiteId', '1']);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
	</script>

	<div class="xrcbplayer">
		<div class="player">
			<div class="label">
				<h1>
					<span class="radio"></span>
					<span class="sep">-</span>
					<span class="podcast"></span>
					<span class="radio-link"></span>
					<span class="podcast-link"></span>
				</h1>
			</div>
			<audio id="xrcbplayer" type="audio/mpeg" controls="controls">
				<p>Your browser doesn't support HTML5 audio. Here is a <a href="">link to the audio</a> instead.</p>
			</audio>
			<div class="logo">XRCB</div>
			<div class="volume"></div>
		</div>
	</div>

	<?php
		if (isset($_GET['playlist']) && $_GET['playlist'] === "radio" &&
			isset($_GET['radiolink']) && $_GET['radiolink'] !== "") {

			// 1. get radio by radiolink
			$radio = $_GET['radio'];
			$radioUrl = $_GET['radiolink'];
			$radioId = url_to_postid($radioUrl);

			if ($radioId > 0) {

				// 2. get podcasts by radio_id
				$podcast_query = array(
			    	'posts_per_page' => '-1',
			    	'post_type' => 'podcast',
			    	//'orderby' => 'post_date',
			    	//'order' => 'DESC',
			    	'meta_query' => array(
			    		'relation' => 'AND',
						array(
							'key'     => 'radio',
							'value'   => $radioId,
							'compare' => '=',
						),
						array(
							'relation' => 'OR',
							array(
								'key'     => 'live',
								'compare' => 'NOT EXISTS',
							),
							array(
								'key'     => 'live',
								'value'   => 'true',
								'compare' => '!=',
							),
						),
					),
			    );
			    $podcast_posts = new WP_Query($podcast_query);

				// 3. echo podcasts as table
				echo "<div class='xrcbplaylist'><table>";

			    while($podcast_posts->have_posts()) {

			    	$podcast_posts->the_post();

			    	$terms = get_the_terms( get_the_ID(), 'podcast_programa' );

					if (get_post_type(get_the_ID()) == 'podcast') {
						$id = get_the_ID();
						$permalink = get_the_permalink();
					} else {
						$id = get_field('podcast')->ID;
						$permalink = get_post_permalink($id);
					}

					$length = wp_get_attachment_metadata(get_post_meta($id, 'file_mp3', true))['length_formatted'];
					$mp3_url = wp_get_attachment_url(get_post_meta($id, 'file_mp3', true));
					$radio_link = get_the_permalink(get_post_meta($id, 'radio', true));
			    ?>

			    	<tr>
			    		<td class="publish_date">
			    			<?php
			    				$date = get_post_meta(get_the_ID(), 'fecha_emision', true);
			    				if ($date != "") {
			    					echo substr($date,6).'/'.substr($date,4,2).'/'.substr($date,2,2);
			    				} else {
			    					echo get_the_date( 'd/m/y' ); 
			    				}
			    			?>
			    		</td>
			        	<td class="name"><a href="<?php echo $permalink; ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a> <?php if (get_field('podcast')) echo '&nbsp;&nbsp;<i class="fa fa-retweet" title="'.get_field('radio', get_field('podcast')->ID)->post_title.'"></i>'; ?></td>
						<td class="length"><?php echo $length; ?></td>
						<td class="mp3"><a class='btn btn-play piwik_download' href='https://<?php echo $_SERVER['SERVER_NAME'] . "/player/index.php?url=" . $mp3_url . "&radio=" . $radio . "&podcast=" . get_the_title() . "&podcastlink=" . $permalink . "&radiolink=" . $radio_link . "&playlist=radio" ?>'>&#9654;</a></td>
					</tr>

				<?php
				}
				echo "</table></div>";
			}
		}
	?>

	<div class="help">
		<h1>Parameters:</h1>
		<ul>
			<li><strong>url</strong>: url of mp3 file to play</li>
			<li><strong>radio</strong>: name of radio station</li>
			<li><strong>radiolink</strong>: link to radio station</li>
			<li><strong>podcast</strong>: name of podcast or file</li>
			<li><strong>podcastlink</strong>: link to the podcast</li>
			<li><strong>autoplay</strong>: start playing when loading page</li>
			<li><strong>logo</strong>: option to personalize logo like <em>logo=no</em>, <em>logo=text</em>, <em>logo=url_image</em> [default: XRCB]</li>
			<li><strong>playlist</strong>: also show playlist, for now only <em>playlist=radio</em> does work and shows all podcasts produced by this radio</li>
		</ul>

		<h1>Example Live Broadcast:</h1>
		<pre>https://player.xrcb.cat/?url=https://icecast.xrcb.cat/main.mp3&podcastlink=https://xrcb.cat/ca/</pre>

		<h1>Example Podcast:</h1>
		<pre>https://player.xrcb.cat/?url=https://xrcb.cat/wp-content/uploads/2018/09/Radioton-01_-Alma-Afrobeat.mp3&radio=XRCB La Comunitaria&radiolink=https://xrcb.cat/ca/radio/la-comunitaria/&podcast=Radiotón 01: Alma Afrobeat&podcastlink=https://xrcb.cat/ca/podcast/radioton-01-alma-afrobeat/</pre>

		<h1>Example Podcast with playlist:</h1>
		<pre>https://player.xrcb.cat/index.php?url=https://xrcb.cat/wp-content/uploads/2023/10/El-Megafono-03.mp3&podcast=03 Cuidar y luchar&podcastlink=https://xrcb.cat/ca/podcast/03-cuidar-y-luchar/&radio=El Megáfono&radiolink=https://xrcb.cat/ca/radio/el-megafono/&playlist=radio</pre>

		<h1>Embed Code:</h1>
		<pre>&lt;iframe width="100%" height="60" src="https://player.xrcb.cat/?url=https://icecast.xrcb.cat/main.mp3" frameborder="0" scrolling="no" style="overflow: hidden; max-width: 100%;"&gt;Iframe no esta soportado en tu navegador.&lt;/iframe&gt;</pre>

		<h1>Embed Example:</h1>
		<a href="https://player.xrcb.cat/iframe.html">iframe.html</a>
	</div>

</body>
</html>
