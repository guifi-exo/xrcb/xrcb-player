# xrcb-player

embed player for https://xrcb.cat based on https://www.mediaelementjs.com/

## Parameters:

- url: url of mp3 file to play
- radio: name of radio station
- podcast: name of podcast or file
- podcastlink: link to the podcast
- autoplay: start playing when loading page
- nologo: hide XRCB logo

## Example Live Broadcast:

`https://player.xrcb.cat/?url=https://icecast.xrcb.cat/main.mp3&podcastlink=https://xrcb.cat/ca/`

## Example Podcast:

`https://player.xrcb.cat/?url=https://xrcb.cat/wp-content/uploads/2018/09/Radioton-01_-Alma-Afrobeat.mp3&radio=XRCB La Comunitaria&podcast=Radiotón 01: Alma Afrobeat&podcastlink=https://xrcb.cat/ca/podcast/radioton-01-alma-afrobeat/`

## Embed Code:

`<iframe width="100%" height="60" src="https://player.xrcb.cat/?url=https://icecast.xrcb.cat/main.mp3" frameborder="0" scrolling="no" style="overflow: hidden;"></iframe>`

## Embed Example:
https://player.xrcb.cat/iframe.html
